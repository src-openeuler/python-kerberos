%global _empty_manifest_terminate_build 0
Name:		python-kerberos
Version:	1.3.1
Release:	2
Summary:	Kerberos high-level interface
License:	Apache-2.0
URL:		https://github.com/apple/ccs-pykerberos
Source0:	https://github.com/apple/ccs-pykerberos/archive/refs/tags/PyKerberos-1.3.1.tar.gz

%description
This Python package is a high-level wrapper for Kerberos (GSSAPI)
operations.  The goal is to avoid having to build a module that wraps
the entire Kerberos.framework, and instead offer a limited set of
functions that do what is needed for client/server Kerberos
authentication based on <http://www.ietf.org/rfc/rfc4559.txt>.
Much of the C-code here is adapted from Apache's mod_auth_kerb-5.0rc7.

%package -n python3-kerberos
Summary:	Kerberos high-level interface
Provides:	python-kerberos
BuildRequires:	python3-devel krb5-devel gcc python3-setuptools
%description -n python3-kerberos
This Python package is a high-level wrapper for Kerberos (GSSAPI)
operations.  The goal is to avoid having to build a module that wraps
the entire Kerberos.framework, and instead offer a limited set of
functions that do what is needed for client/server Kerberos
authentication based on <http://www.ietf.org/rfc/rfc4559.txt>.
Much of the C-code here is adapted from Apache's mod_auth_kerb-5.0rc7.

%package help
Summary:	Development documents and examples for kerberos
Provides:	python3-kerberos-doc
%description help
This Python package is a high-level wrapper for Kerberos (GSSAPI)
operations.  The goal is to avoid having to build a module that wraps
the entire Kerberos.framework, and instead offer a limited set of
functions that do what is needed for client/server Kerberos
authentication based on <http://www.ietf.org/rfc/rfc4559.txt>.
Much of the C-code here is adapted from Apache's mod_auth_kerb-5.0rc7.

%prep
%autosetup -n ccs-pykerberos-PyKerberos-1.3.1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-kerberos -f filelist.lst
%dir %{python3_sitearch}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Jun 13 2024 wangkai <13474090681@163.com> - 1.3.1-2
- License compliance rectification

* Tue Jan 18 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.3.1-1
- Upgrade to version 1.3.1

* Tue Aug 04 2020 wangxiao <wangxiao65@huawei.com>
- package init
